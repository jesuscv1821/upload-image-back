<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Image;

class ImagesController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function all()
    {
        $images = Image::orderBy('created_at', 'desc')->get();
        return $images;
    }

    public function store(Request $request)
    {
        $curl = curl_init( 'https://test.rxflodev.com' );
        $image_file = base64_encode(file_get_contents( $request->file('file')));

        curl_setopt( $curl, CURLOPT_POSTFIELDS, array('imageData' => $image_file) );
        curl_setopt( $curl, CURLOPT_HTTPHEADER, array('Content-Type:multipart/form-data') );
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );

        $result = json_decode(curl_exec($curl));
        curl_close($curl);

        $image_model = new Image();
        $image_model->url = $result->url;
        $image_model->save();

        return $image_model;
    }
}
